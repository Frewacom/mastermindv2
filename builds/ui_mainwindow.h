/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *MainHLayout;
    QFrame *SidebarVFrame;
    QVBoxLayout *SidebarVLayout;
    QPushButton *SB_HomeButton;
    QPushButton *SB_PlayOfflineButton;
    QPushButton *SB_PlayOnlineButton;
    QPushButton *SB_LearnButton;
    QPushButton *SB_ContactButton;
    QPushButton *SB_ToggleChatButton;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *GameWrapperVLayout;
    QHBoxLayout *ContentHLayout;
    QStackedWidget *ContentViews;
    QWidget *Home;
    QVBoxLayout *verticalLayout_3;
    QFrame *HomerHeaderVFrame;
    QVBoxLayout *verticalLayout_2;
    QLabel *HomeHeaderTitle;
    QLabel *HomeHeaderWebsite;
    QVBoxLayout *HomeContentVLayout;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QVBoxLayout *verticalLayout_12;
    QVBoxLayout *HomeNewsVLayout;
    QLabel *HomeNewsTitle;
    QFrame *ArticleVFrame;
    QVBoxLayout *ArticleVLayout;
    QLabel *ArticleTitle;
    QLabel *ArticleContent;
    QSpacerItem *verticalSpacer_3;
    QWidget *Game;
    QVBoxLayout *GameVLayout;
    QFrame *GameHeaderHFrame;
    QHBoxLayout *horizontalLayout_2;
    QLabel *GameTimeElapsed;
    QSpacerItem *horizontalSpacer;
    QLabel *GameAttemptsLeft;
    QVBoxLayout *GameAttemptHistoryVLayout;
    QSpacerItem *verticalSpacer_2;
    QFrame *GameColorBoxHFrame;
    QHBoxLayout *ColorBoxHLayout;
    QPushButton *ColorBox1;
    QPushButton *ColorBox2;
    QPushButton *ColorBox3;
    QPushButton *ColorBox4;
    QPushButton *ColorBoxFinished;
    QFrame *ChatVFrame;
    QVBoxLayout *ChatVLayout;
    QFrame *ChatHeaderHFrame;
    QHBoxLayout *ChatHeaderHLayout;
    QLabel *ChatHeaderLabel;
    QLabel *ChatConnectionStatusLabel;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *ChatHeaderCloseButton;
    QTextEdit *ChatOutput;
    QHBoxLayout *ChatInputHLayout;
    QLineEdit *ChatInput;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1024, 576);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        MainWindow->setMinimumSize(QSize(1024, 576));
        MainWindow->setMaximumSize(QSize(1280, 720));
        QFont font;
        font.setFamily(QStringLiteral("Segoe UI"));
        MainWindow->setFont(font);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        MainHLayout = new QHBoxLayout();
        MainHLayout->setSpacing(0);
        MainHLayout->setObjectName(QStringLiteral("MainHLayout"));
        SidebarVFrame = new QFrame(centralWidget);
        SidebarVFrame->setObjectName(QStringLiteral("SidebarVFrame"));
        SidebarVLayout = new QVBoxLayout(SidebarVFrame);
        SidebarVLayout->setSpacing(0);
        SidebarVLayout->setContentsMargins(11, 11, 11, 11);
        SidebarVLayout->setObjectName(QStringLiteral("SidebarVLayout"));
        SidebarVLayout->setContentsMargins(0, 0, 0, 0);
        SB_HomeButton = new QPushButton(SidebarVFrame);
        SB_HomeButton->setObjectName(QStringLiteral("SB_HomeButton"));

        SidebarVLayout->addWidget(SB_HomeButton);

        SB_PlayOfflineButton = new QPushButton(SidebarVFrame);
        SB_PlayOfflineButton->setObjectName(QStringLiteral("SB_PlayOfflineButton"));

        SidebarVLayout->addWidget(SB_PlayOfflineButton);

        SB_PlayOnlineButton = new QPushButton(SidebarVFrame);
        SB_PlayOnlineButton->setObjectName(QStringLiteral("SB_PlayOnlineButton"));

        SidebarVLayout->addWidget(SB_PlayOnlineButton);

        SB_LearnButton = new QPushButton(SidebarVFrame);
        SB_LearnButton->setObjectName(QStringLiteral("SB_LearnButton"));

        SidebarVLayout->addWidget(SB_LearnButton);

        SB_ContactButton = new QPushButton(SidebarVFrame);
        SB_ContactButton->setObjectName(QStringLiteral("SB_ContactButton"));

        SidebarVLayout->addWidget(SB_ContactButton);

        SB_ToggleChatButton = new QPushButton(SidebarVFrame);
        SB_ToggleChatButton->setObjectName(QStringLiteral("SB_ToggleChatButton"));

        SidebarVLayout->addWidget(SB_ToggleChatButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        SidebarVLayout->addItem(verticalSpacer);


        MainHLayout->addWidget(SidebarVFrame);

        GameWrapperVLayout = new QVBoxLayout();
        GameWrapperVLayout->setSpacing(0);
        GameWrapperVLayout->setObjectName(QStringLiteral("GameWrapperVLayout"));
        ContentHLayout = new QHBoxLayout();
        ContentHLayout->setSpacing(0);
        ContentHLayout->setObjectName(QStringLiteral("ContentHLayout"));
        ContentViews = new QStackedWidget(centralWidget);
        ContentViews->setObjectName(QStringLiteral("ContentViews"));
        ContentViews->setLineWidth(0);
        Home = new QWidget();
        Home->setObjectName(QStringLiteral("Home"));
        verticalLayout_3 = new QVBoxLayout(Home);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        HomerHeaderVFrame = new QFrame(Home);
        HomerHeaderVFrame->setObjectName(QStringLiteral("HomerHeaderVFrame"));
        verticalLayout_2 = new QVBoxLayout(HomerHeaderVFrame);
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 30, 0, 0);
        HomeHeaderTitle = new QLabel(HomerHeaderVFrame);
        HomeHeaderTitle->setObjectName(QStringLiteral("HomeHeaderTitle"));
        QFont font1;
        font1.setFamily(QStringLiteral("Segoe UI Semibold"));
        font1.setBold(true);
        font1.setWeight(75);
        HomeHeaderTitle->setFont(font1);
        HomeHeaderTitle->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout_2->addWidget(HomeHeaderTitle);

        HomeHeaderWebsite = new QLabel(HomerHeaderVFrame);
        HomeHeaderWebsite->setObjectName(QStringLiteral("HomeHeaderWebsite"));
        HomeHeaderWebsite->setAlignment(Qt::AlignHCenter|Qt::AlignTop);

        verticalLayout_2->addWidget(HomeHeaderWebsite);


        verticalLayout_3->addWidget(HomerHeaderVFrame);

        HomeContentVLayout = new QVBoxLayout();
        HomeContentVLayout->setSpacing(0);
        HomeContentVLayout->setObjectName(QStringLiteral("HomeContentVLayout"));
        HomeContentVLayout->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(Home);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 611, 376));
        scrollAreaWidgetContents->setStyleSheet(QStringLiteral(""));
        verticalLayout_12 = new QVBoxLayout(scrollAreaWidgetContents);
        verticalLayout_12->setSpacing(0);
        verticalLayout_12->setContentsMargins(11, 11, 11, 11);
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        verticalLayout_12->setContentsMargins(0, 0, 0, 0);
        HomeNewsVLayout = new QVBoxLayout();
        HomeNewsVLayout->setSpacing(10);
        HomeNewsVLayout->setObjectName(QStringLiteral("HomeNewsVLayout"));
        HomeNewsVLayout->setContentsMargins(20, 10, 20, 0);
        HomeNewsTitle = new QLabel(scrollAreaWidgetContents);
        HomeNewsTitle->setObjectName(QStringLiteral("HomeNewsTitle"));

        HomeNewsVLayout->addWidget(HomeNewsTitle);

        ArticleVFrame = new QFrame(scrollAreaWidgetContents);
        ArticleVFrame->setObjectName(QStringLiteral("ArticleVFrame"));
        ArticleVLayout = new QVBoxLayout(ArticleVFrame);
        ArticleVLayout->setSpacing(0);
        ArticleVLayout->setContentsMargins(11, 11, 11, 11);
        ArticleVLayout->setObjectName(QStringLiteral("ArticleVLayout"));
        ArticleVLayout->setContentsMargins(0, 0, 0, 0);
        ArticleTitle = new QLabel(ArticleVFrame);
        ArticleTitle->setObjectName(QStringLiteral("ArticleTitle"));
        QFont font2;
        font2.setFamily(QStringLiteral("Segoe UI"));
        font2.setBold(false);
        font2.setWeight(50);
        ArticleTitle->setFont(font2);

        ArticleVLayout->addWidget(ArticleTitle);

        ArticleContent = new QLabel(ArticleVFrame);
        ArticleContent->setObjectName(QStringLiteral("ArticleContent"));

        ArticleVLayout->addWidget(ArticleContent);


        HomeNewsVLayout->addWidget(ArticleVFrame);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        HomeNewsVLayout->addItem(verticalSpacer_3);


        verticalLayout_12->addLayout(HomeNewsVLayout);

        scrollArea->setWidget(scrollAreaWidgetContents);

        HomeContentVLayout->addWidget(scrollArea);


        verticalLayout_3->addLayout(HomeContentVLayout);

        verticalLayout_3->setStretch(0, 1);
        verticalLayout_3->setStretch(1, 2);
        ContentViews->addWidget(Home);
        Game = new QWidget();
        Game->setObjectName(QStringLiteral("Game"));
        GameVLayout = new QVBoxLayout(Game);
        GameVLayout->setSpacing(0);
        GameVLayout->setContentsMargins(11, 11, 11, 11);
        GameVLayout->setObjectName(QStringLiteral("GameVLayout"));
        GameVLayout->setContentsMargins(0, 0, 0, 0);
        GameHeaderHFrame = new QFrame(Game);
        GameHeaderHFrame->setObjectName(QStringLiteral("GameHeaderHFrame"));
        sizePolicy.setHeightForWidth(GameHeaderHFrame->sizePolicy().hasHeightForWidth());
        GameHeaderHFrame->setSizePolicy(sizePolicy);
        horizontalLayout_2 = new QHBoxLayout(GameHeaderHFrame);
        horizontalLayout_2->setSpacing(0);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setContentsMargins(10, 10, 10, 10);
        GameTimeElapsed = new QLabel(GameHeaderHFrame);
        GameTimeElapsed->setObjectName(QStringLiteral("GameTimeElapsed"));

        horizontalLayout_2->addWidget(GameTimeElapsed);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        GameAttemptsLeft = new QLabel(GameHeaderHFrame);
        GameAttemptsLeft->setObjectName(QStringLiteral("GameAttemptsLeft"));

        horizontalLayout_2->addWidget(GameAttemptsLeft);


        GameVLayout->addWidget(GameHeaderHFrame);

        GameAttemptHistoryVLayout = new QVBoxLayout();
        GameAttemptHistoryVLayout->setSpacing(0);
        GameAttemptHistoryVLayout->setObjectName(QStringLiteral("GameAttemptHistoryVLayout"));
        GameAttemptHistoryVLayout->setContentsMargins(5, 5, 5, 5);
        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        GameAttemptHistoryVLayout->addItem(verticalSpacer_2);


        GameVLayout->addLayout(GameAttemptHistoryVLayout);

        GameColorBoxHFrame = new QFrame(Game);
        GameColorBoxHFrame->setObjectName(QStringLiteral("GameColorBoxHFrame"));
        ColorBoxHLayout = new QHBoxLayout(GameColorBoxHFrame);
        ColorBoxHLayout->setSpacing(0);
        ColorBoxHLayout->setContentsMargins(11, 11, 11, 11);
        ColorBoxHLayout->setObjectName(QStringLiteral("ColorBoxHLayout"));
        ColorBoxHLayout->setContentsMargins(5, 5, 5, 5);
        ColorBox1 = new QPushButton(GameColorBoxHFrame);
        ColorBox1->setObjectName(QStringLiteral("ColorBox1"));

        ColorBoxHLayout->addWidget(ColorBox1);

        ColorBox2 = new QPushButton(GameColorBoxHFrame);
        ColorBox2->setObjectName(QStringLiteral("ColorBox2"));

        ColorBoxHLayout->addWidget(ColorBox2);

        ColorBox3 = new QPushButton(GameColorBoxHFrame);
        ColorBox3->setObjectName(QStringLiteral("ColorBox3"));

        ColorBoxHLayout->addWidget(ColorBox3);

        ColorBox4 = new QPushButton(GameColorBoxHFrame);
        ColorBox4->setObjectName(QStringLiteral("ColorBox4"));

        ColorBoxHLayout->addWidget(ColorBox4);

        ColorBoxFinished = new QPushButton(GameColorBoxHFrame);
        ColorBoxFinished->setObjectName(QStringLiteral("ColorBoxFinished"));

        ColorBoxHLayout->addWidget(ColorBoxFinished);


        GameVLayout->addWidget(GameColorBoxHFrame);

        ContentViews->addWidget(Game);

        ContentHLayout->addWidget(ContentViews);

        ContentHLayout->setStretch(0, 3);

        GameWrapperVLayout->addLayout(ContentHLayout);

        GameWrapperVLayout->setStretch(0, 1);

        MainHLayout->addLayout(GameWrapperVLayout);

        ChatVFrame = new QFrame(centralWidget);
        ChatVFrame->setObjectName(QStringLiteral("ChatVFrame"));
        ChatVLayout = new QVBoxLayout(ChatVFrame);
        ChatVLayout->setSpacing(0);
        ChatVLayout->setContentsMargins(11, 11, 11, 11);
        ChatVLayout->setObjectName(QStringLiteral("ChatVLayout"));
        ChatVLayout->setContentsMargins(0, 0, 0, 0);
        ChatHeaderHFrame = new QFrame(ChatVFrame);
        ChatHeaderHFrame->setObjectName(QStringLiteral("ChatHeaderHFrame"));
        ChatHeaderHLayout = new QHBoxLayout(ChatHeaderHFrame);
        ChatHeaderHLayout->setSpacing(0);
        ChatHeaderHLayout->setContentsMargins(11, 11, 11, 11);
        ChatHeaderHLayout->setObjectName(QStringLiteral("ChatHeaderHLayout"));
        ChatHeaderHLayout->setContentsMargins(10, 10, 0, 10);
        ChatHeaderLabel = new QLabel(ChatHeaderHFrame);
        ChatHeaderLabel->setObjectName(QStringLiteral("ChatHeaderLabel"));

        ChatHeaderHLayout->addWidget(ChatHeaderLabel);

        ChatConnectionStatusLabel = new QLabel(ChatHeaderHFrame);
        ChatConnectionStatusLabel->setObjectName(QStringLiteral("ChatConnectionStatusLabel"));

        ChatHeaderHLayout->addWidget(ChatConnectionStatusLabel);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        ChatHeaderHLayout->addItem(horizontalSpacer_2);

        ChatHeaderCloseButton = new QPushButton(ChatHeaderHFrame);
        ChatHeaderCloseButton->setObjectName(QStringLiteral("ChatHeaderCloseButton"));

        ChatHeaderHLayout->addWidget(ChatHeaderCloseButton);


        ChatVLayout->addWidget(ChatHeaderHFrame);

        ChatOutput = new QTextEdit(ChatVFrame);
        ChatOutput->setObjectName(QStringLiteral("ChatOutput"));
        ChatOutput->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ChatOutput->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        ChatOutput->setReadOnly(true);

        ChatVLayout->addWidget(ChatOutput);

        ChatInputHLayout = new QHBoxLayout();
        ChatInputHLayout->setSpacing(0);
        ChatInputHLayout->setObjectName(QStringLiteral("ChatInputHLayout"));
        ChatInputHLayout->setContentsMargins(10, -1, 10, 10);
        ChatInput = new QLineEdit(ChatVFrame);
        ChatInput->setObjectName(QStringLiteral("ChatInput"));

        ChatInputHLayout->addWidget(ChatInput);


        ChatVLayout->addLayout(ChatInputHLayout);

        ChatVLayout->setStretch(1, 1);

        MainHLayout->addWidget(ChatVFrame);

        MainHLayout->setStretch(1, 5);
        MainHLayout->setStretch(2, 3);

        gridLayout->addLayout(MainHLayout, 0, 0, 1, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        ContentViews->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Mastermind", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        centralWidget->setAccessibleName(QApplication::translate("MainWindow", "Application", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        SidebarVFrame->setAccessibleName(QApplication::translate("MainWindow", "SidebarFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        SB_HomeButton->setAccessibleName(QApplication::translate("MainWindow", "SB_HomeButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        SB_HomeButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        SB_PlayOfflineButton->setAccessibleName(QApplication::translate("MainWindow", "SB_PlayOfflineButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        SB_PlayOfflineButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        SB_PlayOnlineButton->setAccessibleName(QApplication::translate("MainWindow", "SB_PlayOnlineButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        SB_PlayOnlineButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        SB_LearnButton->setAccessibleName(QApplication::translate("MainWindow", "SB_LearnButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        SB_LearnButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        SB_ContactButton->setAccessibleName(QApplication::translate("MainWindow", "SB_ContactButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        SB_ContactButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        SB_ToggleChatButton->setAccessibleName(QApplication::translate("MainWindow", "SB_ToggleChatButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        SB_ToggleChatButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        HomerHeaderVFrame->setAccessibleName(QApplication::translate("MainWindow", "HomeHeader", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        HomeHeaderTitle->setAccessibleName(QApplication::translate("MainWindow", "HomeHeaderTitleLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        HomeHeaderTitle->setText(QApplication::translate("MainWindow", "Mastermind V<span style=\"color:#1768f0\">2</span>", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        HomeHeaderWebsite->setAccessibleName(QApplication::translate("MainWindow", "HomeHeaderWebsiteLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        HomeHeaderWebsite->setText(QApplication::translate("MainWindow", "http://www.engstrand.design/mastermind", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        scrollArea->setAccessibleName(QApplication::translate("MainWindow", "HomeScrollArea", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        scrollAreaWidgetContents->setAccessibleName(QApplication::translate("MainWindow", "HomeScrollAreaContents", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        HomeNewsTitle->setAccessibleName(QApplication::translate("MainWindow", "HomeContentTitleLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        HomeNewsTitle->setText(QApplication::translate("MainWindow", "Nyheter & Uppdateringar", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ArticleVFrame->setAccessibleName(QApplication::translate("MainWindow", "ArticleFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ArticleTitle->setAccessibleName(QApplication::translate("MainWindow", "ArticleTitleLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ArticleTitle->setText(QApplication::translate("MainWindow", "Mastermind V2", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ArticleContent->setAccessibleName(QApplication::translate("MainWindow", "ArticleContentLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ArticleContent->setText(QApplication::translate("MainWindow", "Helt ny UI och m\303\245nga allm\303\244na f\303\266b\303\244ttringar p\303\245 alla s\303\244tt och vis", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        GameHeaderHFrame->setAccessibleName(QApplication::translate("MainWindow", "GameHeaderFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        GameTimeElapsed->setAccessibleName(QApplication::translate("MainWindow", "GameHeaderLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        GameTimeElapsed->setText(QApplication::translate("MainWindow", "Tid: 13:31:23", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        GameAttemptsLeft->setAccessibleName(QApplication::translate("MainWindow", "GameHeaderLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        GameAttemptsLeft->setText(QApplication::translate("MainWindow", "F\303\266rs\303\266k kvar: 10", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        GameColorBoxHFrame->setAccessibleName(QApplication::translate("MainWindow", "ColorBoxFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ColorBox1->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox1->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBox2->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox2->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBox3->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox3->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBox4->setAccessibleName(QApplication::translate("MainWindow", "ColorBox", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBox4->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ColorBoxFinished->setAccessibleName(QApplication::translate("MainWindow", "ColorBoxFinished", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ColorBoxFinished->setText(QApplication::translate("MainWindow", "Testa", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ChatVFrame->setAccessibleName(QApplication::translate("MainWindow", "ChatFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ChatHeaderHFrame->setAccessibleName(QApplication::translate("MainWindow", "ChatHeaderFrame", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ChatHeaderLabel->setAccessibleName(QApplication::translate("MainWindow", "ChatHeaderLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ChatHeaderLabel->setText(QApplication::translate("MainWindow", "Chatt - ", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ChatConnectionStatusLabel->setAccessibleName(QApplication::translate("MainWindow", "ChatHeaderLabel", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ChatConnectionStatusLabel->setText(QApplication::translate("MainWindow", "Ej ansluten", Q_NULLPTR));
#ifndef QT_NO_ACCESSIBILITY
        ChatHeaderCloseButton->setAccessibleName(QApplication::translate("MainWindow", "ChatHeaderCloseButton", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
        ChatHeaderCloseButton->setText(QString());
#ifndef QT_NO_ACCESSIBILITY
        ChatOutput->setAccessibleName(QApplication::translate("MainWindow", "ChatOutput", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ChatInput->setAccessibleName(QApplication::translate("MainWindow", "ChatInput", Q_NULLPTR));
#endif // QT_NO_ACCESSIBILITY
#ifndef QT_NO_ACCESSIBILITY
        ChatInput->setAccessibleDescription(QString());
#endif // QT_NO_ACCESSIBILITY
        ChatInput->setPlaceholderText(QApplication::translate("MainWindow", "Skriv ett meddelande", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
