#ifndef CHAT_H
#define CHAT_H

#include <QObject>
#include <QTextEdit>

enum Sender
{
    You = 0,
    Opponent = 1
};

class Chat : public QObject
{
    Q_OBJECT
public:
    explicit Chat(QTextEdit *chat, QObject *parent = nullptr);

    void WriteChatMessage(Sender sender, QString msg);

signals:

public slots:

private:
    QTextEdit *m_chat;
};

#endif // CHAT_H
