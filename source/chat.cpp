#include "chat.h"

Chat::Chat(QTextEdit *chat, QObject *parent) : QObject(parent)
{
    m_chat = chat;
}

void Chat::WriteChatMessage(Sender sender, QString msg)
{
    if (sender == Sender::You)
    {
        m_chat->append("<span style='color:#2296F3'>Du: </span>" + msg);
    }
    else
    {
        m_chat->append("<span style='color:#0b48af'>Motspelare: </span>" + msg);
    }
}
