#ifndef HELPERS_H
#define HELPERS_H

#include <QMainWindow>
#include <QGraphicsDropShadowEffect>
#include <QFontDatabase>
#include <QColor>
#include <QFile>

namespace Helpers
{
    void LoadCustomFonts();

    void ApplyStyleSheet(QWidget *centralWidget);
    void ApplyElementMaterialShadow(QWidget *widget);
    void ApplyColorBoxMaterialShadow(QWidget *widget);
}

#endif // HELPERS_H
