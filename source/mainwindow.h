#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "response.h"
#include "helpers.h"
#include "code.h"
#include "chat.h"

#include <QNetworkAccessManager>
#include <QElapsedTimer>
#include <QMainWindow>
#include <QPushButton>
#include <QMouseEvent>
#include <QSettings>
#include <QVariant>
#include <QPixmap>
#include <QEvent>
#include <QDebug>
#include <QTimer>
#include <QLabel>
#include <QTime>
#include <QFile>

enum View
{
    Home = 0,
    Offline = 1,
    Online = 2,
    Learn = 3,
    Contact = 4
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void signal_rightClicked(QObject *sender);

private slots:
    // Timer SLOTS
    void on_TimerUpdate();

    // Chat SLOTS
    void on_ChatSendPressed();
    void on_ChatHeaderCloseButton_clicked();
    void on_SB_ToggleChatButton_clicked();

    // Color box SLOTS
    void on_ColorBoxClicked();
    void on_ColorBoxRightClicked(QObject *sender);
    void on_FinishedClicked();

    // Sidebar SLOTS
    void on_SB_HomeButton_clicked();
    void on_SB_PlayOfflineButton_clicked();
    void on_SB_PlayOnlineButton_clicked();
    void on_SB_LearnButton_clicked();
    void on_SB_ContactButton_clicked();

private:
    Ui::MainWindow *ui;

    // Helper functions
    void m_applyTooltips();
    void m_switchView(int index, QPushButton *button);
    int m_getIndexPropertyAsInt(QPushButton *button);
    QVariant m_convertIndexToQVariant(int index);
    int m_getRandomNumber(int min, int max);

    // Connector functions
    void m_connectMainWindowSignals();
    void m_connectColorBoxes();

    // Events
    bool eventFilter(QObject *watched, QEvent *event);

    // Mastermind functions
    void m_newGame(bool online);
    void m_startTimers();
    void m_setCode(QString c1, QString c2, QString c3, QString c4);
    void m_setRandomSeed();
    void m_setRandomCode();
    void m_writeAttemptHistory(Code *code, Response resp);
    void m_updateColorIndex(QPushButton *button, bool positive);
    void m_updateColor(QPushButton *button);
    void m_updateAttemptCounter();
    int m_calculateScore();
    void m_gameOver();

    // Object pointers
    QNetworkAccessManager *p_webManager;
    QSettings *p_settings;
    Chat *p_chat;
    Code *p_secretCode;

    // Timers
    QTimer *m_timer;
    QElapsedTimer *m_elapsedTimer;
    QTime *m_secondsElapsed;

    // Lists
    QStringList m_colors = {"#FFFFFF", "#1768f0", "#E53935", "#D81B60",
                     "#5E35B1", "#00897B", "#2E7D32", "#FDD835"};

    // Variables
    QPushButton *m_currentViewButton = NULL;

    int m_attemptsLeft = 10;
    int m_numberOfGuesses = 0;

    // Bools
    bool m_hasWon = false;
    bool m_hasStarted = false;
};

#endif // MAINWINDOW_H
