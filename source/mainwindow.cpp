#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QFontDatabase::addApplicationFont(":/fonts/OpenSans-Regular.ttf");
    QFontDatabase::addApplicationFont(":/fonts/OpenSans-Bold.ttf");
    QFontDatabase::addApplicationFont(":/fonts/OpenSans-Light.ttf");

    Helpers::LoadCustomFonts();
    Helpers::ApplyStyleSheet(ui->centralWidget);
    Helpers::ApplyColorBoxMaterialShadow(ui->ColorBox1);
    Helpers::ApplyColorBoxMaterialShadow(ui->ColorBox2);
    Helpers::ApplyColorBoxMaterialShadow(ui->ColorBox3);
    Helpers::ApplyColorBoxMaterialShadow(ui->ColorBox4);
    Helpers::ApplyColorBoxMaterialShadow(ui->ColorBoxFinished);

    p_chat = new Chat(ui->ChatOutput, this);

    m_connectMainWindowSignals();
    m_connectColorBoxes();
    m_applyTooltips();
    m_switchView(View::Home, ui->SB_HomeButton);

//    ui->HomeHeaderTitle->setText("");
//    ui->HomeHeaderWebsite->setText("");
//    QPixmap logo(":/icons/logo.png");
//    ui->HomeHeaderTitle->setPixmap(logo);
//    ui->HomeHeaderTitle->setScaledContents(true);
//    ui->HomeHeaderTitle->setMaximumWidth(512);
//    ui->HomeHeaderTitle->setMaximumHeight(150);

}


// Helper functions
void MainWindow::m_applyTooltips()
{
    ui->SB_HomeButton->setToolTip("Hem");
    ui->SB_PlayOfflineButton->setToolTip("Spela offline");
    ui->SB_PlayOnlineButton->setToolTip("Spela online");
    ui->SB_LearnButton->setToolTip("Hur spelar man?");
    ui->SB_ContactButton->setToolTip("Kontakt");
    ui->SB_ToggleChatButton->setToolTip("Öppna/stäng chat-fönstret");
}

int MainWindow::m_getIndexPropertyAsInt(QPushButton *button)
{
    QVariant index = button->property("index");
    return index.toInt();
}

QVariant MainWindow::m_convertIndexToQVariant(int index)
{
    QVariant variant(index);
    return variant;
}

void MainWindow::m_switchView(int index, QPushButton *button)
{
    ui->ContentViews->setCurrentIndex(index);

    if (m_currentViewButton != NULL)
    {
        m_currentViewButton->setStyleSheet("");
    }

    button->setStyleSheet("background-color: #0d0e0f");
    m_currentViewButton = button;
}

void MainWindow::m_startTimers()
{
    m_timer = new QTimer(this);
    m_elapsedTimer = new QElapsedTimer();
    m_secondsElapsed = new QTime(0,0,0);

    connect(m_timer, SIGNAL(timeout()), this, SLOT(on_TimerUpdate()));
    m_timer->start(1000);
    m_elapsedTimer->start();
}

void MainWindow::m_setRandomSeed()
{
    // Random seed
    QTime seed = QTime::currentTime();
    qsrand((uint)seed.msec());
}

int MainWindow::m_getRandomNumber(int min, int max)
{
    return ((qrand() % ((max + 1) - min)) + min);
}

// Connector functions
void MainWindow::m_connectMainWindowSignals()
{
    connect(ui->ChatInput, SIGNAL(returnPressed()),
            this, SLOT(on_ChatSendPressed()));
}

void MainWindow::m_connectColorBoxes()
{

    connect(ui->ColorBox1, SIGNAL(clicked()), this, SLOT(on_ColorBoxClicked()));
    connect(ui->ColorBox2, SIGNAL(clicked()), this, SLOT(on_ColorBoxClicked()));
    connect(ui->ColorBox3, SIGNAL(clicked()), this, SLOT(on_ColorBoxClicked()));
    connect(ui->ColorBox4, SIGNAL(clicked()), this, SLOT(on_ColorBoxClicked()));

    connect(ui->ColorBoxFinished, SIGNAL(clicked()), this, SLOT(on_FinishedClicked()));

    ui->ColorBox1->setProperty("index", 0);
    ui->ColorBox2->setProperty("index", 0);
    ui->ColorBox3->setProperty("index", 0);
    ui->ColorBox4->setProperty("index", 0);

    m_updateColor(ui->ColorBox1);
    m_updateColor(ui->ColorBox2);
    m_updateColor(ui->ColorBox3);
    m_updateColor(ui->ColorBox4);

    ui->ColorBox1->installEventFilter(this);
    ui->ColorBox2->installEventFilter(this);
    ui->ColorBox3->installEventFilter(this);
    ui->ColorBox4->installEventFilter(this);

    connect(this, SIGNAL(signal_rightClicked(QObject*)), this, SLOT(on_ColorBoxRightClicked(QObject*)));
}


// Events
bool MainWindow::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::MouseButtonPress)
    {
        QMouseEvent *keyEvent = static_cast<QMouseEvent*>(event);
        if (keyEvent->button() == Qt::RightButton)
        {
            emit signal_rightClicked(watched);
            return true;
        }
        else
        {
            return false;
        }
    }
    return false;
}


// Sidebar SLOTS
void MainWindow::on_SB_ToggleChatButton_clicked()
{
    if (ui->ChatVFrame->isVisible())
    {
        ui->ChatVFrame->setVisible(false);
    }
    else
    {
        ui->ChatVFrame->setVisible(true);
    }
}

void MainWindow::on_SB_HomeButton_clicked()
{
    m_switchView(View::Home, ui->SB_HomeButton);
}

void MainWindow::on_SB_PlayOfflineButton_clicked()
{
    m_switchView(View::Offline, ui->SB_PlayOfflineButton);
    m_newGame(false);
}

void MainWindow::on_SB_PlayOnlineButton_clicked()
{
    // Not implemented
}

void MainWindow::on_SB_LearnButton_clicked()
{
    // Not implemented
}

void MainWindow::on_SB_ContactButton_clicked()
{
    // Not implemented
}


// Chat SLOTS
void MainWindow::on_ChatHeaderCloseButton_clicked()
{
    ui->ChatVFrame->setVisible(false);
}

void MainWindow::on_ChatSendPressed()
{
    QString msg = ui->ChatInput->text();
    ui->ChatInput->clear();

    p_chat->WriteChatMessage(Sender::You, msg);
}

// Color box SLOTS
void MainWindow::on_ColorBoxClicked()
{
    QPushButton *button = qobject_cast<QPushButton*>(sender());
    m_updateColorIndex(button, true);
    m_updateColor(button);
}

void MainWindow::on_ColorBoxRightClicked(QObject *sender)
{
    QPushButton *button = qobject_cast<QPushButton*>(sender);
    m_updateColorIndex(button, false);
    m_updateColor(button);
}

void MainWindow::on_FinishedClicked()
{
    int index1 = m_getIndexPropertyAsInt(ui->ColorBox1);
    int index2 = m_getIndexPropertyAsInt(ui->ColorBox2);
    int index3 = m_getIndexPropertyAsInt(ui->ColorBox3);
    int index4 = m_getIndexPropertyAsInt(ui->ColorBox4);

    QString color1 = m_colors[index1];
    QString color2 = m_colors[index2];
    QString color3 = m_colors[index3];
    QString color4 = m_colors[index4];

    Code *code = new Code(color1, color2, color3, color4);

    if (m_attemptsLeft > 0)
    {
        Response resp = code->Compare(p_secretCode);

        m_writeAttemptHistory(code, resp);

        if (resp.Success)
        {
            int score = m_calculateScore();

//            if (scoreIsGreaterThanPreviousHighscores(score))
//            {
//                HighscoreDialog *dialog = new HighscoreDialog(score, this);
//                connect(dialog, SIGNAL(onDialogAccepted(int, QString)),
//                        this, SLOT(onHighscoreAccepted(int, QString)));
//                connect(dialog, SIGNAL(rejected()),
//                        this, SLOT(onHighscoreDismissed()));
//                dialog->show();
//                saveHighscoreToComputer(score);
//            }
//            else
//            {
//                int highscore = loadHighscoreFromComputer();
//                QString text;
//                text += "Grattis, du vann!\n";
//                text += "Tyvärr så slog du inte något rekord den här gången!\n";
//                text += "Ditt personliga bästa är: ";
//                text += QString::number(highscore) + " poäng\n";
//                text += "Den här omgången fick du: ";
//                text += QString::number(score) + " poäng";
//                ShowNewCustomTextDialog(text);
//            }

            // Du vann
            qDebug() << score;
            m_hasWon = true;
            m_hasStarted = false;
        }
        else
        {
            if (m_attemptsLeft <= 0)
            {
                m_gameOver();
            }
        }
    }
}


// Miscellaneous SLOTS
void MainWindow::on_TimerUpdate()
{
    if (!m_hasWon)
    {
        *m_secondsElapsed = m_secondsElapsed->addMSecs(m_timer->interval());
        QString time = m_secondsElapsed->toString("hh:mm:ss");
        ui->GameTimeElapsed->setText(time);
    }
}


// Color box functions
void MainWindow::m_updateColorIndex(QPushButton *button, bool positive)
{
    int index = m_getIndexPropertyAsInt(button);

    if (positive)
    {
        if (index < m_colors.count() - 1)
        {
            index++;
        }
        else
        {
            index = 0;
        }
    }
    else
    {
        if (index <= 0)
        {
            index = m_colors.count() - 1;
        }
        else
        {
            index--;
        }
    }

    button->setProperty("index", m_convertIndexToQVariant(index));
}

void MainWindow::m_updateColor(QPushButton *button)
{
    int index = m_getIndexPropertyAsInt(button);
    button->setStyleSheet("background-color: " + m_colors[index]);
}


// Main game functions
void MainWindow::m_newGame(bool online)
{
    if (!m_hasStarted)
    {
        if (online)
        {

        }
        else
        {
            m_startTimers();
            m_setRandomSeed();
            m_setRandomCode();

            m_hasStarted = true;

            qDebug() << p_secretCode->CodeColors[0];
            qDebug() << p_secretCode->CodeColors[1];
            qDebug() << p_secretCode->CodeColors[2];
            qDebug() << p_secretCode->CodeColors[3];
        }
    }
}

void MainWindow::m_setCode(QString c1, QString c2, QString c3, QString c4)
{
    p_secretCode = new Code(c1, c2, c3, c4);
}

void MainWindow::m_setRandomCode()
{
    QString color1 = m_colors[m_getRandomNumber(0,m_colors.count() - 1)];
    QString color2 = m_colors[m_getRandomNumber(0,m_colors.count() - 1)];
    QString color3 = m_colors[m_getRandomNumber(0,m_colors.count() - 1)];
    QString color4 = m_colors[m_getRandomNumber(0,m_colors.count() - 1)];
    p_secretCode = new Code(color1, color2, color3, color4);
}

void MainWindow::m_updateAttemptCounter()
{
    QString placeholder = "Försök kvar: ";
    QString attemptsLeft = QString::number(m_attemptsLeft);
    ui->GameAttemptsLeft->setText(placeholder + attemptsLeft);
}

int MainWindow::m_calculateScore()
{
    int timeSmall = m_elapsedTimer->elapsed() / 1000;

    if (timeSmall >= 300000)
    {
        timeSmall = 300000;
    }

    int timeScore = 300000 / timeSmall;
    int multiplier = 11 - m_numberOfGuesses;

    int score = timeScore * multiplier;

    return score;
}

void MainWindow::m_writeAttemptHistory(Code *code, Response resp)
{
    m_attemptsLeft -= 1;
    m_numberOfGuesses += 1;
    m_updateAttemptCounter();

    QHBoxLayout *colors = new QHBoxLayout;
    QHBoxLayout *lamps = new QHBoxLayout;

    for (int i = 0; i < code->CodeColors.length(); i++)
    {
        QPushButton *btn = new QPushButton;
        btn->setStyleSheet("background-color:"+code->CodeColors[i]);

        btn->setAccessibleName("ColorBox");
        Helpers::ApplyColorBoxMaterialShadow(btn);

        colors->addWidget(btn);
    }

    for (int colorIndex = 0; colorIndex < resp.Lamps.count(); colorIndex++)
    {
        QFrame *frame = new QFrame;
        frame->setAccessibleName("LampFrame");
        Helpers::ApplyColorBoxMaterialShadow(frame);

        if (resp.Lamps[colorIndex] == "2")
        {
            frame->setStyleSheet("background-color:#2E7D32");
        }
        else if (resp.Lamps[colorIndex] == "1")
        {
            frame->setStyleSheet("background-color:#E53935");
        }
        else
        {
            frame->setStyleSheet("background-color:#FFFFFF");
        }

        lamps->addWidget(frame);
    }

    colors->addLayout(lamps);
    ui->GameAttemptHistoryVLayout->insertLayout(
                ui->GameAttemptHistoryVLayout->count()-1,colors
    );
}

void MainWindow::m_gameOver()
{
    qDebug() << "Du förlorade";
}

// Desctructor
MainWindow::~MainWindow()
{
    delete ui;
}


