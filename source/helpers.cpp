#include "helpers.h"

void Helpers::LoadCustomFonts()
{

}

void Helpers::ApplyStyleSheet(QWidget *centralWidget)
{
    QFile file(":/css/style.css");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    centralWidget->setStyleSheet(styleSheet);
}

void Helpers::ApplyColorBoxMaterialShadow(QWidget *widget)
{
    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect;
    effect->setBlurRadius(8);
    effect->setXOffset(2);
    effect->setYOffset(2);
    effect->setColor(QColor::fromRgb(24, 24, 24));

    widget->setGraphicsEffect(effect);
}

void Helpers::ApplyElementMaterialShadow(QWidget *widget)
{
    QGraphicsDropShadowEffect *effect = new QGraphicsDropShadowEffect;
    effect->setBlurRadius(5);
    effect->setXOffset(1);
    effect->setYOffset(1);
    effect->setColor(QColor::fromRgb(20,20,20));

    widget->setGraphicsEffect(effect);
}

